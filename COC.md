Mastoq

Connect with your friends and others worldwide on Mastoq. A decentralized, free open source, secure social platform without ads and tracking software. English posting allowed only.

Code of Conduct

    ❌ - No racism, sexism, homophobia, transphobia, xenophobia, casteism.
    ❌ - No sexual depictions of minors.
    ❌ - No spam.
    ❌ - No adult/NSFW content.
    ❌ - No bots.
    ❌ - No cross-posting from Twitter.
    ✅ - English posting only.
    ✅ - Media, graphics & artwork.
    ✅ - Memes & funny content.
    ✅ - News & updates.
    ✅ - Other content within our CoC.

In-depth CoC

The following guidelines are not a legal document, and final interpretation is up to the administration of mastoq.com; they are here to provide you with an insight into our content moderation policies:

    The following types of content will be removed from the public timeline:
        Excessive advertising
        Uncurated news bots posting from third-party news sources
        Untagged nudity, pornography and sexually explicit content, including artistic depictions
        Untagged gore and extremely graphic violence, including artistic depictions
    The following types of content will be removed from the public timeline, and may result in account suspension and revocation of access to the service:
        Racism or advocation of racism
        Sexism or advocation of sexism
        Casteism or advocation of casteism
        Discrimination against gender and sexual minorities, or advocation thereof
        Xenophobic and/or violent nationalism
    The following types of content are explicitly disallowed and will result in revocation of access to the service:
        Sexual depictions of children
        Content illegal in Germany, such as holocaust denial or Nazi symbolism
        Conduct promoting the ideology of National Socialism
    Any conduct intended to stalk or harass other users, or to impede other users from utilizing the service, or to degrade the performance of the service, or to harass other users, or to incite other users to perform any of the aforementioned actions, is also disallowed, and subject to punishment up to and including revocation of access to the service. This includes, but is not limited to, the following behaviors:
        Continuing to engage in conversation with a user that has specifically has requested for said engagement with that user to cease and desist may be considered harassment, regardless of platform-specific privacy tools employed.
        Aggregating, posting, and/or disseminating a person's demographic, personal, or private data without express permission (informally called doxing or dropping dox) may be considered harassment.
        Inciting users to engage another user in continued interaction or discussion after a user has requested for said engagement with that user to cease and desist (informally called brigading or dogpiling) may be considered harassment.

These provisions notwithstanding, the administration of the service reserves the right to revoke any user's access permissions, at any time, for any reason, except as limited by law.

Cross-posting

"Cross-posting" is the automatic copying of posts from one platform to another. That definition does not include the simultaneous posting to multiple platforms from the same app, such as Twidere or PostyBirb. If you use those apps, we consider those posts original, and the following need not apply.

Cross-posting from Mastoq to other platforms is fully permitted. Cross-posting into Mastoq from another platform is not permitted.
